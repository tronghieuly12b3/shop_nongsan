@extends('manager.layout.app')

@section('title', 'user')

@section('content')

    @if(session('message'))
        <h1>{{session('message')}} </h1>
    @endif
    <a class="btn btn-primary" href="{{route('user.create')}}">Create User</a>

    <div align="center"><h2> List </h2></div>


    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-icon card-header-rose">
                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                        <tr>
                            <th scope="col">STT</th>
                            <th>name</th>
                            <th>tole</th>
                            <th>image</th>
                            <th>action</th>
                        </tr>
                        </thead>

                        @foreach($users as $user)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$user->name}}</td>
                                <td>
                                    @if($user->roles->count() >0)
                                        @foreach($user->roles as $role)
                                            {{$role->display_name .','}}
                                        @endforeach
                                    @endif
                                </td>
                                <td>

                                    <img width="100px" height="100px" src="{{asset('upload/'.$user->image)}}" alt="">
                                </td>
                                <td>
                                    <a class="btn btn-warning text-white"
                                       href="{{route('user.edit', $user->id)}}">Edit</a>
                                    <form action="{{route('user.destroy', $user->id)}}" method="post"
                                          style="display: inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn" type="submit" style="background-color: #0b68ba">DELETE
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div>
        {{ $users->links() }}
    </div>

@endsection
