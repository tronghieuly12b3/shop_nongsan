@extends('manager.layout.app')

@section('title', 'user')

@section('content')

    <a class="btn btn-primary" href="#">Create Role</a>

    <h1>LIST </h1>

    <table  class="table table-bordered">
        <tr>
            <td>STT</td>
            <td>name</td>
            <td>display name</td>
            <td>action</td>
        </tr>

        @foreach($roles as $item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->display_name}}</td>

                <td>
                    <a class="btn btn-warning text-white" href="#">Edit</a>

                    <form action="#"  method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">DELETE</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>


    <div>
        {{ $roles->links() }}
    </div>

@endsection
