@extends('manager.layout.app')

@section('title', 'Edit categories')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12 ">
            <div class="card ">
                <div class="card-header">Edit Category</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('category.update', $category->id) }}"   >
                        @csrf
                        @method('put')

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $category->name }}"  autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('parent_id') }}</label>

                            <div class="col-md-6">
                                <input id="parent_id" type="text" class="form-control @error('parent_id') is-invalid @enderror" name="parent_id" value="{{ $category->parent_id }}"  autocomplete="parent_id" autofocus>

                                @error('parent_id')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <select class="selectpicker" data-live-search="true" name="categories[]" data-style="select-with-transition" >
                                @foreach($categories as $item)
                                    <option value="{{$item->id}}"  {{$item->id == $category->id ? 'selected' : ''  }}    >{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('role')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


