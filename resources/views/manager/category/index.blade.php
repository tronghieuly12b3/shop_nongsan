@extends('manager.layout.app')

@section('title', 'category')

@section('content')


    <a class="btn btn-primary" href="{{route('category.create')}}">Create Category</a>

    <div align="center"><h2>LIST CATEGORY</h2></div>



    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-icon card-header-rose">
                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
        <tr>
            <th scope="col"><h3 >STT</h3></th>
            <th scope="col"><h3>Loại sản phẩm</h3></th>
            <th scope="col"><h3>Nhóm</h3></th>
            <th scope="col"><h3>Action</h3></th>

        </tr> </thead>
        @foreach($categories as $category)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$category->name}}</td>
                <td>{{$category->parent_id}}</td>

                <td>

                    <a class="btn btn-warning text-white" href="{{route('category.edit', $category->id)}}">Edit</a>
                    <form action="{{route('category.destroy', $category->id)}}"  method="post" style="display: inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn " type="submit"style="background-color: #e91e63">DELETE</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>
                </div>
            </div>
        </div>
    </div>


    <script>

        @if(session('message'))
        alert("{{session('message')}}");
        @endif
    </script>
    <div>
        {{ $categories->links() }}
    </div>

@endsection
