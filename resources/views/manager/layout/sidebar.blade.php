
<div class="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">

    <div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-mini">

        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            Creative Tim
        </a></div>

<div class="sidebar-wrapper">
    <ul class="nav">

        <li class="nav-item active ">
            <a class="nav-link" href="#">
                <i class="material-icons">dashboard</i>
                <p> Dashboard </p>
            </a>
        </li>

        <li class="nav-item  ">
            <a class="nav-link" href="{{route('user.index')}}">
                <i class="material-icons">person</i>
                <p> User </p>
            </a>
        </li>

        <li class="nav-item ">
        <a class="nav-link" href="{{route('category.index')}}">
            <i class="material-icons">content_paste</i>
            <p>Category</p>
        </a>
        </li>

        <li class="nav-item ">
            <a class="nav-link" href="{{route('products.index')}}">
                <i class="material-icons">content_paste</i>
                <p>Product</p>
            </a>
        </li>

        <li class="nav-item ">
            <a class="nav-link" href="{{route('roles.index')}}">
                <i class="material-icons">person</i>
                <p>Role</p>
            </a>
        </li>

    </ul>

</div>
    <div class="sidebar-background"></div>
</div>
