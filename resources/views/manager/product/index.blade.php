@extends('manager.layout.app')

@section('title', 'Product')

@section('content')

    @if(session('message'))
        <h1>{{session('message')}} </h1>
    @endif
    <a class="btn btn-primary" href="{{route('products.create')}}">Create Product</a>

    <h1>LIST </h1>

    <table class="table table-bordered">
        <tr>
            <td>STT</td>
            <td>name</td>
            <td>SKU</td>
            <td>category name</td>
            <td>action</td>
        </tr>

        @foreach($products as $item)
            <tr>

                <td>{{$loop->iteration}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->product_design}}</td>
                <td>
                    @if($item->categories->count() > 0)
                        @foreach($item->categories as $category)
                            {{$category->name .','}}
                        @endforeach
                    @else
                        <h3>khong co category</h3>
                    @endif

                </td>
                <td>
                    <a class="btn btn-warning text-white" href="{{route('products.edit', $item->id)}}">Edit</a>
                    <form action="{{route('products.destroy', $item->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">DELETE</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>


    <div>
        {{ $products   ->links() }}
    </div>

@endsection
