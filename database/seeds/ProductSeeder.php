<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Product::create([
            'name' =>'Rau ngót rừng'
        ]);

        \App\Models\Product::create([
            'name' =>'Ngọn su su'
        ]);
        \App\Models\Product::create([
            'name' =>'Nấm mỡ'
        ]);
        \App\Models\Product::create([
            'name' =>'Nấm bào ngư trắng'
        ]);

        \App\Models\Product::create([
            'name' =>'Nấm kim châm'
        ]);

        \App\Models\Product::create([
            'name' =>'Nho xanh Nam Phi'
        ]);

        \App\Models\Product::create([
            'name' =>'Quả Cherry Mỹ'
        ]);


        \App\Models\Category::create([
            'name'=> 'Rau hữu cơ'
        ]);
        \App\Models\Category::create([
            'name'=> 'Nấm tươi'
        ]);
        \App\Models\Category::create([
            'name'=> 'Hoa quả nhập khẩu'
        ]);
        \App\Models\Category::create([
            'name'=> '	Hoa quả Việt Nam'
        ]);

    }
}
