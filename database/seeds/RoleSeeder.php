<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleManageUser =  \App\Models\Role::updateOrCreate([
            'name'=>'manage-user-in-admin',
            'display_name'=>'Quản lý người dùng',
        ]);

        $roleManageUser =  \App\Models\Role::updateOrCreate([
            'name'=>'manage-category-in-admin',
            'display_name'=>'Quản lý category',
        ]);

        $roleManageUser =  \App\Models\Role::updateOrCreate([
            'name'=>'manage-product-in-admin',
            'display_name'=>'Quản lýproduct',
        ]);
        $roleManageUser =  \App\Models\Role::updateOrCreate([
            'name'=>'manage-order-in-admin',
            'display_name'=>'Quản lý order',
        ]);

        $roleManageUser =  \App\Models\Role::updateOrCreate([
            'name'=>'super-admin',
            'display_name'=>'admin',
        ]);



        $curdUserPermission =  \App\Models\Permission::updateOrCreate([
            'name'=>'curd-user-in-admin',
            'display_name'=>'Quản lý người dùng',
        ]);


        \App\Models\Permission::updateOrCreate([
            'name'=>'can-update-status-order-in-admin',
            'display_name'=>'Thay đổi trạng thái đơn hàng',
        ]);

        $roleManageUser->givePermissionTo($curdUserPermission->name);


        $userAdmin =  \App\User::where('email','huhuhu11@gmail.com')->first();


        if(!$userAdmin)
        {
            $userAdmin = \App\User::create([
                'name' =>'Shop nongsan',
                'email' => 'huhuhu11@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'phone' => '01262624111',
                'address'=>'',
                'gender'=> 1,
                'image'=>''
            ]);
        }

        if(!$userAdmin->hasRole('super-admin'))
        {
            $userAdmin->assignRole('super-admin');
        }

    }

}
