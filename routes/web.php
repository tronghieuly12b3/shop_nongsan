<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware'=>'auth'],function(){


Route::group(['prefix'=>'manager', 'namespace'=>'manager'],function(){

    Route::resource('user', 'UserController');
    Route::resource('category', 'CategoryController');
    Route::resource('roles', 'RoleController');
    Route::resource('products', 'ProductController');
});
Route::get('/', function () {
    return view('manager.dashboard.index');
});

});
Auth::routes();



