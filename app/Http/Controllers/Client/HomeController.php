<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;

class HomeController extends Controller
{
    protected $categoryModel;
    protected $productModel;

    /**
     * CategoryController constructor.
     * @param $categoryModel
     */
    public function __construct(Category $categoryModel,Product $product)
    {
        $this->categoryModel = $categoryModel;
        $this->productModel = $product;
    }


    public function index()
    {

        $products =  $this->productModel->latest('id')->get();
        $categories = $this->categoryModel->with(['childCategories'])->whereNull('parent_id')->latest('id')->get();
        return view('client.home.index', compact('products','categories'));
    }

    public function showProduct($id)
    {

        $products =  $this->productModel
            ->whereHas('categories', function ($query) use($id){
                $query->where('category_id', $id);
            })
            ->latest('id')->get();
        return view('client.home.show-product', compact('products'));
    }

}
