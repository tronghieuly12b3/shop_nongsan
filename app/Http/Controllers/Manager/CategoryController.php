<?php

namespace App\Http\Controllers\Manager;

use App\Http\Resources\Resource\CategoryResource;
use App\Models\Category;
use App\Models\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\category\CreateCategoryRequest;
use App\Http\Requests\category\UpdateCategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    protected $categoryModel;
    protected $productModel;


    public function __construct(Category $categoryModel,Product $product)
    {
        $this->categoryModel = $categoryModel;
        $this->productModel = $product;

    }

    public function index()
    {

        $categories = $this->categoryModel->with(['childCategories', 'parentCategories'])
            ->latest('id')->paginate(5);

        return view('manager.category.index', compact('categories'));

    }


    public function create()
    {
        $categories =  $this->categoryModel->get(['id', 'name']);

        return view('manager.category.create', compact('categories'));


    }



    public function store(Request $request)
    {
        $dataCreate =  $request->all();
        $this->categoryModel->create($dataCreate);

        return redirect()->route('category.index')->with('message', 'tao category thanh cong');

    }


    public function show($id)
    {
        $category = $this->categoryModel->findOrFail($id);

        return  response()->json([
            'status'=>200,
            'data' =>new CategoryResource($category)
        ]);

    }




    public function edit($id)
    {
        $category= Category::findOrFail($id);
        $categories = $this->categoryModel->get(['id','name']);
        return view('manager.category.edit')->with(['category'=> $category,'categories'=>$categories]);
    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);


        $dataUpdate = $request->all();


        $category->update($dataUpdate);

        return redirect()->route('category.index')->with('message', 'Edit category thanh cong');
    }


    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        //Thực hiện chuyển trang
        return redirect()->route('category.index')->with('message', 'Delete product success');
    }
}
