<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'product_design'=>'required',
            'sale'=>'required',
            'image' =>'nullable|mimes:jpg,bmp,png|file|image',
            'price'=>'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' =>'Tên không được để trống',
            'product_design'=>'Mã sp không được để trống',
            'sale.required' =>'Sale không được để trống',
            'image.required' =>'Image không được để trống',
            'price.required' =>'Price không được để trống',
        ];
    }
}

