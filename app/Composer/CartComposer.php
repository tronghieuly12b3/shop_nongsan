<?php


namespace App\Composer;
use App\Models\Cart;
use Illuminate\View\View;

class CartComposer
{
    protected $cart;


    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }


    public function compose(View $view)
    {

        $cartTotal = $this->cart->where('user_id', auth()->user()->id)->count();
        $view->with(['cartTotal' => $cartTotal]);
    }


}
