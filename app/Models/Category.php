<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{

    protected $table = 'categories';

    protected $fillable = [
        'name', 'parent_id'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_product', 'category_id', 'product_id');
    }

    public function syncProducts($productIds)
    {
        return $this->products()->sync($productIds);
    }

    public function childCategories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parentCategories()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }
}
